//Logical Operators

void main(){
	int x=10;
	int y=8;
	print(x && y);	//Error: A value of type 'int' can't be assigned to a variable of type 'bool'.

	print(x || y);	//Error: A value of type 'int' can't be assigned to a variable of type 'bool'.

	print(!x);	//Error: A value of type 'int' can't be assigned to a variable of type 'bool'.

	print(!y);	//Error: A value of type 'int' can't be assigned to a variable of type 'bool'.

	}
