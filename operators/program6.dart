// Logical operators
void main(){
	int x=10;
	int y=8;
	
	print((++x < ++y) && (--x > ++x));	//(x->11 < y->9)	-> false 
	
	int a=5;
	int b=6;
	
	print((a++ < ++b) || (a-- > ++b));	//(5(a->6) < b->7)	-> true
	
	print(x);		//11
	print(y);		//9
	print(a);		//6
	print(b);		//7
	}
